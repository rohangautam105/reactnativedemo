# pelocal_mobile

## Development Setup

* Installing dependencies

  ```
  You will need Node, the React Native command line interface, a JDK, and Android Studio.  
  you will need to install Android Studio in order to set up the necessary tooling to build your React Native app for Android.
  ```

* Install Node 12 or newer.
* Install and set up Java Development Kit

  ```
  Project requires at least the version 8 of the Java SE Development Kit (JDK)
  ```

* Install Android Studio

  ```
  the following items are checked:
  ```

* Android SDK
* Android SDK Platform
* Android Virtual Device

  ```
  Select the "SDK Platforms" tab from within the SDK Manager, then check the box next to "Show Package Details" in the bottom right corner. Look for and expand the Android 10 (Q) entry, then make sure the following items are checked:
  ```

  ```
  Android SDK Platform 29
  Intel x86 Atom_64 System Image or Google APIs Intel x86 Atom System Image 
  Next, select the "SDK Tools" tab and check the box next to "Show Package Details" here as well. Look for and expand the "Android SDK Build-Tools" entry, then make sure that 29.0.2 is selected.
  ```
  ```
  Finally, click "Apply" to download and install the Android SDK and related build tools.
  ```

* Configure the ANDROID_HOME environment variable

  ```
  Add the following lines to your $HOME/.bash_profile or $HOME/.bashrc (if you are using zsh then ~/.zprofile or ~/.zshrc) config file:
  ```
  ```
  export ANDROID_HOME=$HOME/Android/Sdk
  export PATH=$PATH:$ANDROID_HOME/emulator
  export PATH=$PATH:$ANDROID_HOME/tools
  export PATH=$PATH:$ANDROID_HOME/tools/bin
  export PATH=$PATH:$ANDROID_HOME/platform-tools
  ```

##How to run in a nutshell.

*  Install `yarn`
*  Use `yarn install` from root directory to install dependencies. 
*  To start the metro server use : `yarn start`.
*  To run the project use : `yarn android` or `yarn ios`.

