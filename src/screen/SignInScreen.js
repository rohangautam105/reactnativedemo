import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {addUser} from '../store/reducers/userSlice';
import {withTheme, TextInput, Button, HelperText} from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './Style';

const SignInScreen = props => {
  const {navigation} = props;
  const [emailText, setEmailText] = useState('');
  const [passwordText, setPasswordText] = useState('');
  const [passwordFieldSecure, setPasswordFieldSecure] = useState(true);
  const [emailValidation, setEmailValidation] = useState(false);
  const [passwordValidation, setPasswordValidation] = useState(false);
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const {userDetail} = useSelector(state => state.userSlice);
  const dispatch = useDispatch();

  const onSubmit = () => {
    console.log('asdada', userDetail);
    if (emailText === '') {
      setEmailValidation(true);
      setPasswordValidation(false);
    } else if (passwordText === '') {
      setEmailValidation(false);
      setPasswordValidation(true);
    } else {
      setEmailValidation(false);
      setPasswordValidation(false);
      console.log('************', emailText, passwordText, userDetail);
      const isUserValid = userDetail.some(user => {
        return (
          user.email === emailText.toLowerCase() &&
          user.password === passwordText
        );
      });
      if (isUserValid) {
        setShowErrorMessage(false);
        dispatch(addUser({email: emailText, password: passwordText}));
      } else {
        setShowErrorMessage(true);
      }
    }
  };

  return (
    <View style={[styles.container]}>
      <Text style={{fontSize: 20}}>Sign In</Text>
      {showErrorMessage ? (
        <Text style={{color: 'red'}}>User not exits</Text>
      ) : (
        <></>
      )}
      <TextInput
        placeholder="Email ID"
        value={emailText}
        style={[
          {marginVertical: 5, width: 200},
          // GlobalStyles.fontFamilyNunitoRegular,
        ]}
        onChangeText={text => setEmailText(text)}
        left={
          <TextInput.Icon
            name={props => <MaterialIcons name="alternate-email" {...props} />}
            color="grey"
          />
        }
      />
      {emailValidation ? (
        <HelperText type="error" visible style={styles.errorText}>
          * Email required
        </HelperText>
      ) : (
        <></>
      )}
      <TextInput
        placeholder="Password"
        style={[
          {marginVertical: 5, width: 200},
          // GlobalStyles.fontFamilyNunitoRegular,
        ]}
        left={<TextInput.Icon name="lock-outline" color="grey" />}
        right={
          <TextInput.Icon
            name={passwordFieldSecure ? 'eye-off' : 'eye'}
            color="grey"
            onPress={() => setPasswordFieldSecure(prev => !prev)}
          />
        }
        value={passwordText}
        secureTextEntry={passwordFieldSecure}
        onChangeText={text => setPasswordText(text)}
      />
      {passwordValidation ? (
        <HelperText type="error" visible style={styles.errorText}>
          * Password required
        </HelperText>
      ) : (
        <></>
      )}
      <Button
        icon="camera"
        mode="contained"
        style={[
          {marginVertical: 5},
          // GlobalStyles.fontFamilyNunitoRegular,
        ]}
        onPress={onSubmit}>
        Sign In
      </Button>
      <Text onPress={() => navigation.navigate('SignUpScreen')}>
        Don't have an account? Sign Up
      </Text>
    </View>
  );
};

export default SignInScreen;
