import React from 'react';
import {View, Text} from 'react-native';
import {
  withTheme,
  TextInput,
  Button,
  HelperText,
  Card,
  Title,
  Paragraph,
  Avatar,
} from 'react-native-paper';
import {useDispatch} from 'react-redux';
import {clearUserStore} from '../store/reducers/userSlice';
import styles from './Style';

const HomeScreen = props => {
  const {} = props;
  const dispatch = useDispatch();

  const onSubmit = () => {
    dispatch(clearUserStore());
  };

  const LeftContent = props => <Avatar.Icon {...props} icon="folder" />;

  return (
    <View style={[styles.container]}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between' }}>
        <Card>
          <Card.Content>
            <Title>tinder</Title>
          </Card.Content>
          <Card.Cover source={{uri: 'https://picsum.photos/700'}} />
        </Card>
        <Card>
          <Card.Content>
            <Title>bumble</Title>
          </Card.Content>
          <Card.Cover source={{uri: 'https://picsum.photos/700'}} />
        </Card>
      </View>
      <Button
        icon="camera"
        mode="contained"
        style={[
          {marginVertical: 5},
          // GlobalStyles.fontFamilyNunitoRegular,
        ]}
        onPress={onSubmit}>
        Logout
      </Button>
    </View>
  );
};

export default HomeScreen;
