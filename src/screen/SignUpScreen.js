import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {Formik, Form} from 'formik';
import {useSelector, useDispatch} from 'react-redux';
import * as Yup from 'yup';
import {TextInput, Button, HelperText} from 'react-native-paper';
import {addUserList, addUser} from '../store/reducers/userSlice';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './Style';

const AddUserSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email address').required('Required'),
  password: Yup.string()
    .required('* Required')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character',
    ),
  confirm_password: Yup.string()
    .required('* Required')
    .oneOf([Yup.ref('password'), null], 'Passwords must match'),
});

const SignUpScreen = props => {
  const {navigation} = props;
  const dialogLabelName = {
    label: {
      email: 'Email ID',
      password: 'Password',
      comfirmPassword: 'Confirm Password',
    },
  };

  const [userEmailAddress] = useState('');
  const [userPassword] = useState('');
  const [userConfirmPassword] = useState('');
  const [passwordFieldSecure1, setPasswordFieldSecure1] = useState(true);
  const [passwordFieldSecure2, setPasswordFieldSecure2] = useState(true);
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const {userDetail} = useSelector(state => state.userSlice);
  const dispatch = useDispatch();

  const handleFormSubmit = values => {
    console.log('***********', values);
    const isUserValid = userDetail.some(user => {
      return user.email === values.email.toLowerCase();
    });
    if (isUserValid) {
      setShowErrorMessage(true);
    } else {
      setShowErrorMessage(false);
      dispatch(addUserList({ email: values.email.toLowerCase(), password: values.password }));
      dispatch(addUser({email: values.email, password: values.password}));
    }
  };

  return (
    <View style={[styles.container]}>
      <Text>Sign Up</Text>
      {showErrorMessage ? (
        <Text style={{color: 'red'}}>User Already exits</Text>
      ) : (
        <></>
      )}
      <Formik
        initialValues={{
          email: userEmailAddress,
          password: userPassword,
          confirm_password: userConfirmPassword,
        }}
        validationSchema={AddUserSchema}
        onSubmit={values => handleFormSubmit(values)}>
        {({
          values,
          errors,
          isValid,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <>
            <TextInput
              placeholder={dialogLabelName.label.email}
              value={values.email}
              onChangeText={val => setFieldValue('email', val)}
              onBlur={() => setFieldTouched('email')}
              style={[
                {marginVertical: 5, width: 200},
                // GlobalStyles.fontFamilyNunitoRegular,
              ]}
              left={
                <TextInput.Icon
                  name={props => (
                    <MaterialIcons name="alternate-email" {...props} />
                  )}
                  color="grey"
                />
              }
            />
            {errors.email && touched.email ? (
              <HelperText type="error" visible style={styles.errorText}>
                {errors.email}
              </HelperText>
            ) : null}
            <TextInput
              placeholder={dialogLabelName.label.password}
              value={values.password}
              secureTextEntry={passwordFieldSecure1}
              onChangeText={val => setFieldValue('password', val)}
              onBlur={() => setFieldTouched('password')}
              style={[
                {marginVertical: 5, width: 200},
                // GlobalStyles.fontFamilyNunitoRegular,
              ]}
              left={<TextInput.Icon name="lock-outline" color="grey" />}
              right={
                <TextInput.Icon
                  name={passwordFieldSecure1 ? 'eye-off' : 'eye'}
                  color="grey"
                  onPress={() => setPasswordFieldSecure1(prev => !prev)}
                />
              }
            />
            {errors.password && touched.password ? (
              <HelperText type="error" visible style={styles.errorText}>
                {errors.password}
              </HelperText>
            ) : null}
            <TextInput
              placeholder={dialogLabelName.label.password}
              value={values.confirm_password}
              secureTextEntry={passwordFieldSecure2}
              style={[
                {marginVertical: 5, width: 200},
                // GlobalStyles.fontFamilyNunitoRegular,
              ]}
              onChangeText={val => setFieldValue('confirm_password', val)}
              onBlur={() => setFieldTouched('confirm_password')}
              left={<TextInput.Icon name="lock-outline" color="grey" />}
              right={
                <TextInput.Icon
                  name={passwordFieldSecure2 ? 'eye-off' : 'eye'}
                  color="grey"
                  onPress={() => setPasswordFieldSecure2(prev => !prev)}
                />
              }
            />
            {errors.confirm_password && touched.confirm_password ? (
              <HelperText type="error" visible style={styles.errorText}>
                {errors.confirm_password}
              </HelperText>
            ) : null}
            <Button
              icon="camera"
              mode="contained"
              style={[
                {marginVertical: 5},
                // GlobalStyles.fontFamilyNunitoRegular,
              ]}
              onPress={handleSubmit}>
              Sign Up
            </Button>
          </>
        )}
      </Formik>
      <Text onPress={() => navigation.goBack()}>have an account? Sign In</Text>
    </View>
  );
};

export default SignUpScreen;
