import React from 'react';
import {useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import UnAuthRoutes from './unAuth/UnAuthRoutes';
import AuthRoutes from './auth/authRoutes';

const Stack = createNativeStackNavigator();

const RouterManager = () => {
  const {user} = useSelector(state => state.userSlice);

  const getUserDetail = () => {
    if (user) {
      return true;
    }
    return false;
  };

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="RouteManager"
        screenOptions={{
          header: () => null,
          headerShown: false,
        }}>
        {
          getUserDetail() ? (
            <Stack.Screen name="AuthRutes" component={AuthRoutes} />
          ) : (
            <Stack.Screen name="UnAuthRoutes" component={UnAuthRoutes} />
          )
          // <Stack.Screen name='UnAuthRoutes' component={UnAuthRoutes} />
        }
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RouterManager;
