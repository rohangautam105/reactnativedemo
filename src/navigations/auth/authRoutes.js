import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../../screen/HomeScreen';

const Stack = createNativeStackNavigator();

const authRoutes = () => (
  <Stack.Navigator
    initialRouteName="authRoutes"
    screenOptions={{
      header: () => null,
      headerShown: false,
    }}>
    <Stack.Screen name="HomeScreen" component={HomeScreen} />
  </Stack.Navigator>
);

export default authRoutes;
