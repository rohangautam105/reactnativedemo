import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SignInScreen from '../../screen/SignInScreen';
import SignUpScreen from '../../screen/SignUpScreen';

const Stack = createNativeStackNavigator();

const UnAuthRoutes = () => (
  <Stack.Navigator
    initialRouteName="UnAuthRoutes"
    screenOptions={{
      header: () => null,
      headerShown: false,
    }}>
    <Stack.Screen name="SignInScreen" component={SignInScreen} />
    <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
  </Stack.Navigator>
);

export default UnAuthRoutes;
