import {combineReducers} from 'redux';
import userSlice from './reducers/userSlice';
import appSlice from './reducers/appSlice';

const appReducer = combineReducers({
  userSlice: userSlice,
  appSlice: appSlice,
});

export default appReducer;
