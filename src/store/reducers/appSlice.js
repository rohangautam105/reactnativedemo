/* eslint-disable no-param-reassign */
import {createSlice} from '@reduxjs/toolkit';

export const appSlice = createSlice({
  name: 'counter',
  initialState: {
    counter: 0,
  },
  reducers: {
    incrementCounter: state => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.counter += 1;
    },
  },
});

// Action creators are generated for each case reducer function
export const {incrementCounter} = appSlice.actions;

export default appSlice.reducer;
