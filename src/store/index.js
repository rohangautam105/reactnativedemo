import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RootReducer from '../store/RootReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['userSlice'], // which reducer want to store
};

const persistedReducer = persistReducer(persistConfig, RootReducer);

const configureStore = () => {
  const allMiddleWares = applyMiddleware(thunk);

  const reduxStore = createStore(persistedReducer, allMiddleWares);

  const persistor = persistStore(reduxStore);

  return {store: reduxStore, persistor};
};

export default configureStore;
